﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManagerMenu : MonoBehaviour {

	public Image light;
	public GameObject[] AllMenu;
	// Use this for initialization
	void Start () {
		ShowMenuByTag ("MENU_INFO");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ShowMenuByTag (string tag){
		for (var i = 0; i < AllMenu.Length; ++i) {
			if(AllMenu[i].tag == tag){
				if(tag == "MENU_CHALLENGE"){
					AllMenu [i].GetComponent<Challenge> ().ShowPlayer ();
				}
				AllMenu[i].SetActive(true);
			}else{
				AllMenu[i].SetActive(false);
			}
		}
	}
	
	public void PlayButtonOnClick(){
		ShowMenuByTag ("MENU_CHALLENGE");
	}
	public void HomeButtonOnClick(){
	}
	public void LeaderButtonOnClick(){
	}
	public void FriendButtonOnClick(){
	}
	public void GalleryButtonOnClick(){
	}
	public void OtherButtonOnClick(){
	}
	public void AddChallengeButtonOnClick(){
	}
	public void NextButtonOnClick(){
	}
	public void CloseButtonOnClick(){
		ShowMenuByTag ("MENU_INFO");
	}
}
