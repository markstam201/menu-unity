﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Challenge : MonoBehaviour {

	public PlayerChallenge player1;
	public PlayerChallenge player2;
	public PlayerChallenge player3;
	public PlayerChallenge player4;

	//public GameObject ButtonAddChallenge1;
//	public GameObject ButtonAddChallenge2;
	//public GameObject ButtonAddChallenge3;
	//public GameObject ButtonAddChallenge4;
	// Use this for initialization
	void Start () {

	}
	public void ShowPlayer(){
		player1.ShowInfo ();
		player2.HideInfo ();
		player3.ShowInfo ();
		player4.HideInfo ();
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void NextButtonOnClick(){
		Debug.Log ("Click button next");
		var a = Random.Range (1, 5);
		if (a == 1) {
			player1.ShowInfo();
			player1.SetTextName("Player" + Random.Range(1, 100));
		}
		if (a == 2) {
			player2.ShowInfo();
			player2.SetTextName("Player" + Random.Range(1, 200));
		}
		if (a == 3) {
			player3.ShowInfo();
			player3.SetTextName("Player" + Random.Range(1, 200));
		}
		if (a == 4) {
			player4.ShowInfo();
			player4.SetTextName("Player" + Random.Range(1, 200));
		}
	}

	public void CloseButtonOnClick(){
		Application.LoadLevel("MenuInfo");
	}

	public void ChallengeOnClick(){
		Debug.Log ("Click button Challenge");
	}
}
