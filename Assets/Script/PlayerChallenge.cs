﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerChallenge : MonoBehaviour {
	public Image BgAvatar;
	public Image Avatar;
	public Image StrokeAvatar;
	public Text NamePlayer;
	public Text Description;
	public Text Level;
	public GameObject ButtonAddChallenge;

	// Use this for initialization
	void Start () {
		//this.ShowInfo ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowInfo(){
		ButtonAddChallenge.SetActive(false);
		this.enabled = true;
		BgAvatar.enabled = true;
		Avatar.enabled = true;
		StrokeAvatar.enabled = true;
		NamePlayer.enabled = true;
		Description.enabled = true;
		Level.enabled = true;
	}

	public void HideInfo(){
		ButtonAddChallenge.SetActive(true);
		this.enabled = false;
		BgAvatar.enabled = false;
		Avatar.enabled = false;
		StrokeAvatar.enabled = false;
		NamePlayer.enabled = false;
		Description.enabled = false;
		Level.enabled = false;
	}

	public void SetTextName(string name){
		NamePlayer.text = name;
	}

	public void SetTextDes(string des){
		Description.text = des;
	}

	public void SetTextLevel(string level){
		Level.text = level;
	}

	public void SetAvatar(){
		//Avatar == ??
	}
}
