﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Info : MonoBehaviour {
	public Image Avatar;
	public Text Name;
	public Text Des;
	public Image[] CupCoffee;
	// Use this for initialization
	void Start () {
		for (var i = 0; i < CupCoffee.Length; i++) {
		//	CupCoffee[i].enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}	

	public void SetName(string name){
		Name.text = name;
	}

	public void SetDes(string des){
		Des.text = des;
	}
	public void SetAvatar(){
		//Avatar = ???
	}
	public void ShowCupCoffe(int number){
		for (var i = 0; i < number; i++) {
			CupCoffee[i].enabled = true;
		}
	}
}
